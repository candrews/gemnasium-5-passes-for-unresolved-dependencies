# gemnasium-5-passes-for-unresolved-dependencies

Test project for https://gitlab.com/gitlab-org/gitlab/-/issues/467050

This project demonstrates that, for a gradle project with unresolvable dependencies, Gemnasium 5 will pass (which is bad and a regression) while Gemnasium 4 will fail.

This project contains a simple Gradle project with an unresolvable dependency.

The GitLab CI pipeline demonstrates the problem. Both the `gemnasium-maven-4` and `gemnasium-maven-5` jobs are expected to fail, but only the  `gemnasium-maven-4` job actually does.

With Gemasium 4, the scan fails, as it should:
```
$ docker run -e SECURE_LOG_LEVEL=debug -e -it -v "$(pwd):/app:Z" -w /app registry.gitlab.com/security-products/gemnasium-maven:4.17.1 /analyzer sbom  Using java version 'adoptopenjdk-17.0.8+101'
[INFO] [gemnasium-maven] [2024-06-11T17:20:03Z] [/go/src/app/cmd/gemnasium-maven/main.go:56] ▶ GitLab gemnasium-maven analyzer v4.17.1
[DEBU] [gemnasium-maven] [2024-06-11T17:20:03Z] [/go/pkg/mod/gitlab.com/gitlab-org/security-products/analyzers/common/v3@v3.2.3/cacert/cacert.go:65] ▶ CA cert bundle not imported: empty bundle or empty target path
[DEBU] [gemnasium-maven] [2024-06-11T17:20:03Z] [/go/src/app/finder/finder.go:64] ▶ inspect directory: .
[DEBU] [gemnasium-maven] [2024-06-11T17:20:03Z] [/go/src/app/finder/finder.go:96] ▶ skip ignored directory: .gradle
[DEBU] [gemnasium-maven] [2024-06-11T17:20:03Z] [/go/src/app/finder/detect.go:84] ▶ Selecting gradle for maven because this is the first match
[INFO] [gemnasium-maven] [2024-06-11T17:20:03Z] [/go/src/app/finder/finder.go:116] ▶ Detected supported dependency files in '.'. Dependency files detected in this directory will be processed. Dependency files in other directories will be skipped.
[DEBU] [gemnasium-maven] [2024-06-11T17:20:03Z] [/go/src/app/cmd/gemnasium-maven/main.go:251] ▶ Exporting dependencies for /app/build.gradle
[DEBU] [gemnasium-maven] [2024-06-11T17:20:29Z] [/go/src/app/builder/gradle/gradle.go:115] ▶ /app/gradlew --init-script /gemnasium-init.gradle gemnasiumDumpDependencies
Downloading https://services.gradle.org/distributions/gradle-8.7-all.zip
.....................10%.....................20%......................30%.....................40%......................50%.....................60%.....................70%......................80%.....................90%......................100%

Welcome to Gradle 8.7!

Here are the highlights of this release:
 - Compiling and testing with Java 22
 - Cacheable Groovy script compilation
 - New methods in lazy collection properties

For more details see https://docs.gradle.org/8.7/release-notes.html

Starting a Gradle Daemon (subsequent builds will be faster)
> Task :gemnasiumDumpDependencies FAILED

FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':gemnasiumDumpDependencies'.
> Project has 1 unresolved dependencies: doesnot:exist:1.0

* Try:
> Run with --stacktrace option to get the stack trace.
> Run with --info or --debug option to get more log output.
> Run with --scan to get full insights.
> Get more help at https://help.gradle.org.

BUILD FAILED in 25s
1 actionable task: 1 executed

exit status 1
[1]
```

With Gemnasium 5, the scan passes, creating an SBOM that is very misleading:
```
$ docker run -e SECURE_LOG_LEVEL=debug -e -it -v "$(pwd):/app:Z" -w /app registry.gitlab.com/security-products/gemnasium-maven:5.2.8 /analyzer sbom  
Using java version 'adoptopenjdk-17.0.8+101'
[INFO] [gemnasium-maven] [2024-06-11T17:18:21Z] [/go/src/app/cmd/gemnasium-maven/main.go:56] ▶ GitLab gemnasium-maven analyzer v5.2.8
[DEBU] [gemnasium-maven] [2024-06-11T17:18:21Z] [/go/pkg/mod/gitlab.com/gitlab-org/security-products/analyzers/common/v3@v3.2.3/cacert/cacert.go:65] ▶ CA cert bundle not imported: empty bundle or empty target path
[DEBU] [gemnasium-maven] [2024-06-11T17:18:21Z] [/go/src/app/finder/finder.go:64] ▶ inspect directory: .
[DEBU] [gemnasium-maven] [2024-06-11T17:18:21Z] [/go/src/app/finder/finder.go:96] ▶ skip ignored directory: .gradle
[DEBU] [gemnasium-maven] [2024-06-11T17:18:21Z] [/go/src/app/finder/detect.go:84] ▶ Selecting gradle for maven because this is the first match
[INFO] [gemnasium-maven] [2024-06-11T17:18:21Z] [/go/src/app/finder/finder.go:116] ▶ Detected supported dependency files in '.'. Dependency files detected in this directory will be processed. Dependency files in other directories will be skipped.
[DEBU] [gemnasium-maven] [2024-06-11T17:18:21Z] [/go/src/app/cmd/gemnasium-maven/main.go:251] ▶ Exporting dependencies for /app/build.gradle
[DEBU] [gemnasium-maven] [2024-06-11T17:18:46Z] [/go/src/app/builder/gradle/gradle.go:95] ▶ /app/gradlew --init-script /gemnasium-init.gradle htmlDependencyReport
Downloading https://services.gradle.org/distributions/gradle-8.7-all.zip
.....................10%.....................20%......................30%.....................40%......................50%.....................60%.....................70%......................80%.....................90%......................100%

Welcome to Gradle 8.7!

Here are the highlights of this release:
 - Compiling and testing with Java 22
 - Cacheable Groovy script compilation
 - New methods in lazy collection properties

For more details see https://docs.gradle.org/8.7/release-notes.html

Starting a Gradle Daemon (subsequent builds will be faster)

> Task :htmlDependencyReport
See the report at: file:///app/build/reports/project/dependencies/index.html

BUILD SUCCESSFUL in 24s
1 actionable task: 1 executed

[DEBU] [gemnasium-maven] [2024-06-11T17:18:46Z] [/go/src/app/advisory/repo.go:124] ▶ /usr/bin/git -C /gemnasium-db remote set-url origin https://gitlab.com/gitlab-org/security-products/gemnasium-db.git

[DEBU] [gemnasium-maven] [2024-06-11T17:18:48Z] [/go/src/app/advisory/repo.go:124] ▶ /usr/bin/git -C /gemnasium-db fetch --force --tags origin master
From https://gitlab.com/gitlab-org/security-products/gemnasium-db
 * branch                  master     -> FETCH_HEAD
   e5be1aeec3..35e05fae32  master     -> origin/master

[DEBU] [gemnasium-maven] [2024-06-11T17:18:48Z] [/go/src/app/advisory/repo.go:124] ▶ /usr/bin/git -C /gemnasium-db checkout master
Already on 'master'
Your branch is behind 'origin/master' by 37 commits, and can be fast-forwarded.
  (use "git pull" to update your local branch)

[DEBU] [gemnasium-maven] [2024-06-11T17:18:48Z] [/go/src/app/advisory/repo.go:137] ▶ /usr/bin/git -C /gemnasium-db symbolic-ref -q HEAD
[DEBU] [gemnasium-maven] [2024-06-11T17:18:48Z] [/go/src/app/advisory/repo.go:145] ▶ /usr/bin/git -C /gemnasium-db reset --hard origin/master
HEAD is now at 35e05fae32 Merge branch 'advng/packagist/composer/composer/CVE-2024-35242' into 'master'

[DEBU] [gemnasium-maven] [2024-06-11T17:18:48Z] [/go/src/app/advisory/repo.go:153] ▶ /usr/bin/git -C /gemnasium-db rev-parse HEAD
35e05fae32f43c11ec4703c4e16a01392597adbf

[INFO] [gemnasium-maven] [2024-06-11T17:18:48Z] [/go/src/app/advisory/repo.go:157] ▶ Using commit 35e05fae32f43c11ec4703c4e16a01392597adbf
 of vulnerability database

[DEBU] [gemnasium-maven] [2024-06-11T17:18:48Z] [/go/src/app/scanner/scanner.go:131] ▶ Location set to build.gradle
[DEBU] [gemnasium-maven] [2024-06-11T17:18:48Z] [/go/src/app/vrange/cli/cli.go:93] ▶ /vrange/semver/vrange-linux maven /tmp/vrange_queries3078257034
[]
```